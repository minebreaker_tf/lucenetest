package test;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.gosen.GosenAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

public final class Japanese {

    public static void main(String[] args) throws Exception {

        Analyzer analyzer = new GosenAnalyzer();

        Directory directory = new RAMDirectory();
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        IndexWriter iwriter = new IndexWriter(directory, config);
        Document doc = new Document();
        doc.add(new Field("title", "高慢と偏見", TextField.TYPE_STORED));
        doc.add(new Field("text", "相当の財産をもっている独身の男なら、きっと奥さんをほしがっているにちがいないということは、世界のどこへ行っても通る真理である。", TextField.TYPE_STORED));
        iwriter.addDocument(doc);
        Document doc2 = new Document();
        doc2.add(new Field("title", "吾輩は猫である", TextField.TYPE_STORED));
        doc2.add(new Field("text", "「奥さん奥さん、月並の標本が来ましたぜ。月並もあのくらいになるとなかなか振っていますなあ。さあ遠慮はいらんから、存分御笑いなさい」", TextField.TYPE_STORED));
        iwriter.addDocument(doc2);
        iwriter.close();

        DirectoryReader ireader = DirectoryReader.open(directory);
        IndexSearcher isearcher = new IndexSearcher(ireader);
        QueryParser parser = new QueryParser("text", analyzer);

        Query query = parser.parse("独身");
        ScoreDoc[] hits = isearcher.search(query, 100).scoreDocs;
        System.out.println("Length: " + hits.length);
        for (ScoreDoc hit : hits) {
            Document hitDoc = isearcher.doc(hit.doc);
            System.out.println("Hit: " + hitDoc.get("text"));
        }

        Query query2 = parser.parse("奥さん");
        ScoreDoc[] hits2 = isearcher.search(query2, 100).scoreDocs;
        System.out.println("Length: " + hits2.length);
        for (ScoreDoc hit : hits2) {
            Document hitDoc = isearcher.doc(hit.doc);
            System.out.println("Hit: " + hitDoc.get("text"));
        }

        ireader.close();
        directory.close();
    }

}
