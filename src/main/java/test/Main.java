package test;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

public final class Main {

    public static void main(String[] args) throws Exception {

        Analyzer analyzer = new StandardAnalyzer();

        Directory directory = new RAMDirectory();
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        IndexWriter iwriter = new IndexWriter(directory, config);

        Document doc = new Document();
        doc.add(new Field("author", "Kevlin Henney", TextField.TYPE_STORED));
        doc.add(new Field(
                "text",
                "A common fallacy is to assume authors of incomprehensible code will be able to express themselves clearly in comments.",
                TextField.TYPE_STORED
        ));
        iwriter.addDocument(doc);

        Document doc2 = new Document();
        doc2.add(new Field("author", "Steve McConnell", TextField.TYPE_STORED));
        doc2.add(new Field("text", "Good code is its own best documentation.", TextField.TYPE_STORED));
        iwriter.addDocument(doc2);
        iwriter.close();

        DirectoryReader ireader = DirectoryReader.open(directory);
        IndexSearcher isearcher = new IndexSearcher(ireader);
        QueryParser parser = new QueryParser("text", analyzer);

        Query query = parser.parse("fallacy");
        ScoreDoc[] hits = isearcher.search(query, 100).scoreDocs;
        System.out.println("Length: " + hits.length);
        for (ScoreDoc hit : hits) {
            Document hitDoc = isearcher.doc(hit.doc);
            System.out.println(hitDoc.get("author"));
        }

        Query query2 = parser.parse("code");
        ScoreDoc[] hits2 = isearcher.search(query2, 100).scoreDocs;
        System.out.println("Length: " + hits2.length);
        for (ScoreDoc hit : hits2) {
            Document hitDoc = isearcher.doc(hit.doc);
            System.out.println(hitDoc.get("author"));
        }

        ireader.close();
        directory.close();

    }

}
